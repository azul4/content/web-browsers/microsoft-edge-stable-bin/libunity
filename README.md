# libunity

Required for download progress on KDE

Library for instrumenting and integrating with all aspects of the Unity shell

https://launchpad.net/libunity

<br>

How to clone this repository:
```
git clone https://gitlab.com/azul4/content/web-browsers/microsoft-edge-stable-bin/libunity.git
```

